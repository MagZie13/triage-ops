# frozen_string_literal: true

module Labels
  COMMUNITY_CONTRIBUTION_LABEL = 'Community contribution'
  LEADING_ORGANIZATION_LABEL = 'Leading Organization'

  UX_LABEL = 'UX'
  FRONTEND_LABEL = 'frontend'
  BACKEND_LABEL = 'backend'

  DOCUMENTATION_LABEL = 'documentation'
  TECHNICAL_WRITING_LABEL = 'Technical Writing'
  TECHNICAL_WRITING_TRIAGED_LABEL = 'tw::triaged'

  RUN_FULL_RSPEC_LABEL = 'pipeline:run-full-rspec'

  HACKATHON_LABEL = 'Hackathon'

  WORKFLOW_IN_DEV_LABEL = 'workflow::in dev'
  WORKFLOW_READY_FOR_REVIEW_LABEL = 'workflow::ready for review'

  IDLE_LABEL = 'idle'
  STALE_LABEL = 'stale'

  AUTOMATION_AUTHOR_REMINDED_LABEL = 'automation:author-reminded'
  AUTOMATION_REVIEWERS_REMINDED_LABEL = 'automation:reviewers-reminded'

  FEDRAMP_VULNERABILITY_LABEL = 'FedRAMP Milestone::Vuln Remediation'
  VULNERABILITY_SLA_LABEL = 'Vulnerability SLA'

  TYPE_LABELS = [
    'type::feature',
    'type::maintenance',
    'type::bug'
  ].freeze

  TYPE_IGNORE_LABEL = 'type::ignore'

  SUBTYPE_LABELS = [
    'bug::performance',
    'bug::availability',
    'bug::vulnerability',
    'bug::mobile',
    'bug::functional',
    'bug::ux',
    'feature::addition',
    'feature::enhancement',
    'feature::consolidation',
    'feature::removal',
    'maintenance::refactor',
    'maintenance::dependency',
    'maintenance::usability',
    'maintenance::test-gap',
    'maintenance::pipelines',
    'maintenance::workflow',
    'maintenance::scalability'
  ].freeze

  SPECIAL_ISSUE_LABELS = [
    'support request',
    'meta',
    'triage report'
  ].freeze

  # Govern:Threat Insights labels
  THREAT_INSIGHTS_GROUP_LABEL = 'group::threat insights'
  THREAT_INSIGHTS_TEAM_LABELS = [
    'threat insights::navy',
    'threat insights::tangerine'
  ].freeze

  SPAM_LABEL = 'Spam'

  MASTER_BROKEN_LABEL = 'master:broken'
  MASTER_FOSS_BROKEN_LABEL = 'master:foss-broken'
  PIPELINE_EXPEDITE_MASTER_FIXING_LABEL = 'pipeline:expedite-master-fixing'
end
