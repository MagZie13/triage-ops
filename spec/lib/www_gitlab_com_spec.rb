# frozen_string_literal: true

require 'spec_helper'
require_relative '../../lib/www_gitlab_com'

RSpec.describe WwwGitLabCom, :http_requests_allowed do
  describe '.sections' do
    it 'fetches sections from www-gitlab-com data' do
      expect(described_class.sections.keys).to include('dev')
    end
  end

  describe '.stages' do
    it 'fetches stages from www-gitlab-com data' do
      expect(described_class.stages.keys).to include('manage')
    end
  end

  describe '.groups' do
    it 'fetches groups from www-gitlab-com' do
      expect(described_class.groups.keys).to include('source_code')
    end
  end

  describe '.categories' do
    it 'fetches categories from www-gitlab-com' do
      expect(described_class.categories.keys).to include('projects')
    end
  end

  describe '.team_from_www' do
    it 'fetches team from www-gitlab-com' do
      expect(described_class.team_from_www['sytses'].keys).to include('departments')
    end
  end

  describe '.roulette' do
    it 'fetches roulette data' do
      expect(described_class.roulette[0].keys).to include('username')
    end
  end

  describe '.distribution_projects' do
    it 'fetches distribution projects' do
      expect(described_class.distribution_projects).to include(4359271) # https://gitlab.com/gitlab-org/build/cng
    end
  end
end
