# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../triage/processor/community/growth_affecting_notifier'

RSpec.describe Triage::GrowthAffectingNotifier do
  include_context 'with event', 'Triage::IssuableEvent' do
    let(:event_attrs) do
      {
        object_kind: 'merge_request',
        action: 'open',
        project_id: project_id,
        iid: merge_request_iid,
        from_gitlab_org?: true
      }
    end
  end

  let(:merge_request_changes) do
    {
      'changes' => [
        {
          "old_path" => "app/views/admin/application_settings/_account_and_limit.html.haml",
          "new_path" => "something/new.md"
        }
      ]
    }
  end

  let(:label_names) { ['Community contribution'] }

  include_context 'with merge request notes'

  before do
    stub_api_request(
      path: "/projects/#{project_id}/merge_requests/#{merge_request_iid}/changes",
      response_body: merge_request_changes)
  end

  subject { described_class.new(event) }

  include_examples 'registers listeners', ['merge_request.open', 'merge_request.update']

  describe '#applicable?' do
    it_behaves_like 'community contribution open resource #applicable?'

    include_examples 'event is applicable'

    context 'when there was no change to relevant files' do
      let(:merge_request_changes) do
        {
          'changes' => [
            {
              "old_path" => "something/old.md",
              "new_path" => "something/new.md"
            }
          ]
        }
      end

      include_examples 'event is not applicable'
    end

    context 'when there is already a comment for the same purpose' do
      let(:merge_request_notes) do
        [
          { body: 'review comment 1' },
          { body: comment_mark }
        ]
      end

      include_examples 'event is not applicable'
    end
  end

  describe '#documentation' do
    it_behaves_like 'processor documentation is present'
  end

  describe '#process' do
    it 'posts a comment' do
      body = add_automation_suffix('community/growth_affecting_notifier.rb') do
        <<~MARKDOWN.chomp
          #{subject.__send__(:unique_comment).__send__(:hidden_comment)}
          @s_awezec @kniechajewicz @doniquesmit @csouthard this merge request touches files that could potentially affect user growth or subscription cost management.
        MARKDOWN
      end

      expect_comment_request(event: event, body: body) do
        subject.process
      end
    end
  end
end
