# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/processor/ux_mrs'
require_relative '../../triage/triage/event'

RSpec.describe Triage::UxMrs do
  include_context 'with slack posting context'
  include_context 'with event', 'Triage::MergeRequestEvent' do
    let(:event_attrs) do
      {
        from_gitlab_org?: true,
        by_team_member?: true,
        wip?: false,
        url: url,
        title: title
      }
    end

    let(:url) { 'http://gitlab.com/mr_url' }
    let(:title) { 'Merge request title' }
    let(:added_label_names) { [described_class::UX_LABEL] }
  end

  subject { described_class.new(event, messenger: messenger_stub) }

  before do
    stub_api_request(
      path: "/projects/#{event.project_id}/#{event.object_kind}s/#{event.iid}/notes",
      query: { per_page: 100 },
      response_body: [])
  end

  include_examples 'registers listeners', ["merge_request.update", "merge_request.open"]

  it_behaves_like 'processor slack options', '#ux-mrs'

  include_examples 'applicable on contextual event'

  describe '#applicable?' do
    context 'when event project is not under gitlab-org' do
      before do
        allow(event).to receive(:from_gitlab_org?).and_return(false)
      end

      include_examples 'event is not applicable'
    end

    context 'when event is not from a team member' do
      before do
        allow(event).to receive(:by_team_member?).and_return(false)
      end

      include_examples 'event is not applicable'
    end

    context 'when resource is not opened' do
      before do
        allow(event).to receive(:resource_open?).and_return(false)
      end

      include_examples 'event is not applicable'
    end

    context 'when event is a Draft' do
      before do
        allow(event).to receive(:wip?).and_return(true)
      end

      include_examples 'event is not applicable'
    end

    context 'when other label is added' do
      let(:added_label_names) { ['bug'] }

      include_examples 'event is not applicable'
    end

    context 'when no label is added' do
      let(:added_label_names) { [] }

      include_examples 'event is not applicable'
    end
  end

  describe '#documentation' do
    it_behaves_like 'processor documentation is present'
  end

  describe '#process' do
    before do
      allow(messenger_stub).to receive(:ping)
    end

    shared_examples 'process UX merge request' do
      it 'posts a comment' do
        body = add_automation_suffix('ux_mrs.rb') do
          <<~MARKDOWN.chomp
            #{subject.__send__(:unique_comment).__send__(:hidden_comment)}
            Please wait for Reviewer Roulette to suggest a designer for UX review, and then assign them as Reviewer. This helps evenly distribute reviews across UX.
          MARKDOWN
        end

        expect_comment_request(event: event, body: body) do
          subject.process
        end
      end
    end

    it_behaves_like 'process UX merge request'
    it_behaves_like 'slack message posting' do
      before do
        allow(subject).to receive(:post_ux_comment)
      end

      let(:message_body) do
        <<~MARKDOWN
          Hi UX team, an MR is ready for UX review: (#{title}) - #{url}.
        MARKDOWN
      end
    end
  end
end
