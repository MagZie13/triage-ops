# frozen_string_literal: true

module Triage
  class Listener
    ISSUABLE_ACTIONS = %w[open close update reopen note].freeze
    ALLOWED_EVENT_DEFS = {
      'issue' => ISSUABLE_ACTIONS,
      'merge_request' => ISSUABLE_ACTIONS + %w[merge approval unapproval approved unapproved]
    }.freeze

    ListenerResourceNotAllowedError = Class.new(StandardError)
    ListenerActionNotAllowedError = Class.new(StandardError)

    attr_reader :resource, :action

    def self.listeners_for_event(resource, action)
      case resource
      when '*' # we set this processor for all resources
        case action
        when '*' # and all actions
          ALLOWED_EVENT_DEFS.flat_map do |resource, actions|
            listeners_for_actions(resource, actions)
          end
        else
          ALLOWED_EVENT_DEFS.each_key.flat_map do |actual_resource|
            listeners_for_actions(actual_resource, [action])
          end
        end
      else
        case action
        when '*'
          listeners_for_actions(resource, ALLOWED_EVENT_DEFS[resource])
        else
          listeners_for_actions(resource, [action])
        end
      end
    end

    def self.listeners_for_actions(resource, actions)
      [resource].product(actions).map { |resource, action| new(resource, action) }
    end

    def self.allowed_resources
      ALLOWED_EVENT_DEFS.keys + ['*']
    end

    def self.allowed_resource?(resource)
      allowed_resources.include?(resource)
    end

    def self.allowed_actions_for_resource(resource)
      ALLOWED_EVENT_DEFS[resource] + ['*']
    end

    def self.allowed_action_for_resource?(resource, action)
      allowed_actions_for_resource(resource).include?(action)
    end

    def initialize(resource, action)
      raise ListenerResourceNotAllowedError, "Resource #{resource} is not allowed. Allowed resources are #{self.class.allowed_resources}" unless self.class.allowed_resource?(resource)
      raise ListenerActionNotAllowedError, "Action #{action} is not allowed. Allowed actions are #{self.class.allowed_actions_for_resource(resource)}" unless self.class.allowed_action_for_resource?(resource, action)

      @resource = resource
      @action = action
    end

    def event
      "#{resource}.#{action}"
    end
  end
end
