# frozen_string_literal: true

require_relative 'community_processor'

module Triage
  class CodeReviewExperienceFeedback < CommunityProcessor
    react_to 'merge_request.merge', 'merge_request.close'

    def applicable?
      wider_community_contribution? &&
        unique_comment.no_previous_comment? &&
        not_spam?
    end

    def process
      post_review_experience_comment
    end

    def documentation
      <<~TEXT
        This processor asks for feedback from community contributors about the code review experience.
      TEXT
    end

    private

    def post_review_experience_comment
      add_comment(message.strip, append_source_link: true)
    end

    def message
      comment = <<~MARKDOWN.chomp
        @#{event.resource_author.username}, how was your code review experience with this merge request? Please tell us how we can continue to [iterate](https://about.gitlab.com/handbook/values/) and improve:

        1. React with a :thumbsup: or a :thumbsdown: on this comment to describe your experience.
        1. Create a new comment starting with `@gitlab-bot feedback` below, and leave any additional feedback you have for us in the comment.

        Interested in learning more tips and tricks to solve your next challenge faster? [Subscribe to the GitLab Community Newsletter](https://about.gitlab.com/community/newsletter/) for contributor-focused content and opportunities to level up.

        Thanks for your help! :heart:
      MARKDOWN

      unique_comment.wrap(comment)
    end

    def not_spam?
      !event.label_names.include?(Labels::SPAM_LABEL)
    end
  end
end
